This repository contains a reproduction case for a confusing error
message produced by dune.

Software versions:
- ocaml 4.14.0
- dune 3.6.1

Running `sh build.sh` calls `ocamlc` directly, and produces the
expected error message:

```
$ sh build.sh
File "b.ml", line 1:
Error: The implementation b.ml does not match the interface b.cmi: 
       Type declarations do not match:
         type t = { b : bool; a : A.t; }
       is not included in
         type t = { a : A.t; b : bool; }
       Field a has been moved from position 1 to 2.
       File "b.mli", lines 1-4, characters 0-1: Expected declaration
       File "b.ml", lines 1-4, characters 0-1: Actual declaration
```

On the other hand, running `dune build @all` produces an error message where the two occurences of the module A in the example are tagged with different stamps, suggesting that the .ml and .mli files are referencing distinct modules (they are not).

```
$ dune build @all
File "b.ml", line 1:                
Error: The implementation b.ml
       does not match the interface .test.objs/byte/test__B.cmi: 
       Type declarations do not match:
         type t = { b : bool; a : A/2.t; }
       is not included in
         type t = { a : A/1.t; b : bool; }
       Field a has been moved from position 1 to 2.
       File "b.mli", lines 1-4, characters 0-1: Expected declaration
       File "b.ml", lines 1-4, characters 0-1: Actual declaration
```

(The problem is with `A/2.t` and `A/1.t` in the source fragments quoted.)

(This smells like a problem caused by Dune's approach to build libraries in isolation and detect transitive dependencies, but note that both modules A and B involved are part of the same dune library.)
